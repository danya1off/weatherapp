//
//  AppDelegate.swift
//  WeatherApp
//
//  Created by Jeyhun Danyalov on 7/11/18.
//  Copyright © 2018 Jeyhun Danyalov. All rights reserved.
//

import UIKit
import CoreData
import CocoaLumberjack

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        DDLog.add(DDTTYLogger.sharedInstance)
        
        UINavigationBar.appearance().tintColor = UIColor.foregroundColor
        UINavigationBar.appearance().barTintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.foregroundColor, NSAttributedString.Key.font: UIFont.helveticaBold(ofSize: 17)]
        UINavigationBar.appearance().largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.foregroundColor, NSAttributedString.Key.font: UIFont.helveticaCondensedBlack(ofSize: 30)]
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        window?.rootViewController = UINavigationController(rootViewController: WeatherListTVC())
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {}

    func applicationDidEnterBackground(_ application: UIApplication) {}

    func applicationWillEnterForeground(_ application: UIApplication) {}

    func applicationDidBecomeActive(_ application: UIApplication) {}

    func applicationWillTerminate(_ application: UIApplication) {
        CustomContext.shared.saveContext()
    }

    

}

