//
//  CustomCell.swift
//  WeatherApp
//
//  Created by Jeyhun Danyalov on 7/14/18.
//  Copyright © 2018 Jeyhun Danyalov. All rights reserved.
//

import UIKit

class CustomCell: UITableViewCell {
    
    var viewModel: CellViewModel? {
        didSet {
            if let viewModel = viewModel {
                weatherTitle.text = viewModel.weatherTitle
                weatherDesc.text = viewModel.weatherItem.weather[0].description.capitalized
                temperature.text = viewModel.weatherItem.main.temperature.celcius
                dateLabel.text = viewModel.weatherItem.date
                weatherIcon.image = UIImage(named: viewModel.weatherItem.weather[0].icon)
            }
        }
    }
    
    let bgView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        return view
    }()
    
    let separator: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.backgroundColor
        return view
    }()
    
    let weatherIcon: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        imageView.tintColor = UIColor.foregroundColor
        return imageView
    }()
    
    var weatherTitle: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.helveticaBold(ofSize: 20)
        label.textColor = UIColor.foregroundColor
        return label
    }()
    
    var weatherDesc: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.helveticaRegular(ofSize: 14)
        label.textColor = UIColor.foregroundColor
        return label
    }()
    
    var temperature: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.helveticaBold(ofSize: 40)
        label.textColor = UIColor.foregroundColor
        label.textAlignment = .right
        return label
    }()
    
    var dateLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.helveticaRegular(ofSize: 14)
        label.textColor = UIColor.foregroundColor
        label.textAlignment = .right
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupCell()
    }
    
    private func setupCell() {
        backgroundColor = UIColor.white
        addSubview(bgView)
        bgView.addSubview(weatherIcon)
        bgView.addSubview(weatherTitle)
        bgView.addSubview(weatherDesc)
        bgView.addSubview(temperature)
        bgView.addSubview(dateLabel)
        bgView.addSubview(separator)
        setupConstraints()
    }
    
    private func setupConstraints() {
        
        NSLayoutConstraint.activate([
            
            bgView.centerXAnchor.constraint(equalTo: centerXAnchor),
            bgView.centerYAnchor.constraint(equalTo: centerYAnchor),
            bgView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.9),
            bgView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.9),
            
            weatherIcon.heightAnchor.constraint(equalTo: bgView.heightAnchor, multiplier: 0.3),
            weatherIcon.widthAnchor.constraint(equalTo: weatherIcon.heightAnchor, multiplier: 1),
            weatherIcon.leadingAnchor.constraint(equalTo: bgView.leadingAnchor, constant: 25),
            weatherIcon.topAnchor.constraint(equalTo: bgView.topAnchor,
                                             constant: UIDevice.current.userInterfaceIdiom == .phone ? 20 : 40),
            
            weatherTitle.leadingAnchor.constraint(equalTo: weatherIcon.leadingAnchor),
            weatherTitle.topAnchor.constraint(equalTo: weatherIcon.bottomAnchor, constant: 15),
            
            weatherDesc.leadingAnchor.constraint(equalTo: weatherTitle.leadingAnchor),
            weatherDesc.topAnchor.constraint(equalTo: weatherTitle.bottomAnchor),
            
            temperature.topAnchor.constraint(equalTo: weatherIcon.topAnchor),
            temperature.trailingAnchor.constraint(equalTo: bgView.trailingAnchor, constant: -25),
            
            dateLabel.trailingAnchor.constraint(equalTo: temperature.trailingAnchor),
            dateLabel.topAnchor.constraint(equalTo: weatherDesc.topAnchor),
            
            separator.heightAnchor.constraint(equalToConstant: 1),
            separator.bottomAnchor.constraint(equalTo: bottomAnchor),
            separator.leadingAnchor.constraint(equalTo: weatherIcon.leadingAnchor),
            separator.trailingAnchor.constraint(equalTo: temperature.trailingAnchor),
            
            ])
        
    }
    
    
    
    
    
    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
