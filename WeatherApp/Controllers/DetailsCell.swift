//
//  DetailsCell.swift
//  WeatherApp
//
//  Created by Jeyhun Danyalov on 7/14/18.
//  Copyright © 2018 Jeyhun Danyalov. All rights reserved.
//

import UIKit

class DetailsCell: UITableViewCell {
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    var iconImage: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        imageView.tintColor = UIColor.foregroundColor
        return imageView
    }()
    
    var detailsTitle: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor.foregroundColor
        label.font = UIFont.helveticaRegular(ofSize: 14)
        return label
    }()
    
    var detailsValue: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor.foregroundColor
        label.font = UIFont.helveticaBold(ofSize: 14)
        return label
    }()
    
    private func setup() {
        
        backgroundColor = .clear
        addSubview(iconImage)
        addSubview(detailsTitle)
        addSubview(detailsValue)
        
        // setup constraints
        NSLayoutConstraint.activate([
            
            iconImage.centerYAnchor.constraint(equalTo: centerYAnchor),
            iconImage.leadingAnchor.constraint(equalTo: leadingAnchor),
            iconImage.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.5),
            iconImage.widthAnchor.constraint(equalTo: iconImage.heightAnchor, multiplier: 1),
            
            detailsTitle.centerYAnchor.constraint(equalTo: centerYAnchor),
            detailsTitle.leadingAnchor.constraint(equalTo: iconImage.trailingAnchor, constant: 10),
            
            detailsValue.centerYAnchor.constraint(equalTo: centerYAnchor),
            detailsValue.trailingAnchor.constraint(equalTo: trailingAnchor),
            
            
            ])
    }
    
    
    
    
    
    
    
    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
