//
//  WeatherDetailsVC.swift
//  WeatherApp
//
//  Created by Jeyhun Danyalov on 7/14/18.
//  Copyright © 2018 Jeyhun Danyalov. All rights reserved.
//

import UIKit

class WeatherDetailsVC: UIViewController {
    
    var viewModel: CellViewModel
    
    init(viewModel: CellViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        /*
        I put this parameter in here, because it didn't worked in setup func.
        I have no idea why it's working only in viewDidLoad before super.viewDidLoad()
        */
        navigationItem.largeTitleDisplayMode = .never
        super.viewDidLoad()
        setup()
    }
    
}

// MARK: UITableViewDelegate/UITableViewDataSource
extension WeatherDetailsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    // creating static cell in tableView
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.detailsCellID, for: indexPath) as! DetailsCell
        if indexPath.row == 0 {
            cell.iconImage.image = #imageLiteral(resourceName: "max_temp")
            cell.detailsTitle.text = "Max. temperature"
            cell.detailsValue.text = viewModel.weatherItem.main.maxTemperature.celcius
        } else if indexPath.row == 1 {
            cell.iconImage.image = #imageLiteral(resourceName: "min_temp")
            cell.detailsTitle.text = "Min. temperature"
            cell.detailsValue.text = viewModel.weatherItem.main.minTemperature.celcius
        } else if indexPath.row == 2 {
            cell.iconImage.image = #imageLiteral(resourceName: "presure")
            cell.detailsTitle.text = "Presure level"
            cell.detailsValue.text = "\(viewModel.weatherItem.main.pressure.rounded)mbar"
        } else if indexPath.row == 3 {
            cell.iconImage.image = #imageLiteral(resourceName: "humidity")
            cell.detailsTitle.text = "Humidity"
            cell.detailsValue.text = "\(viewModel.weatherItem.main.humidity)%"
        } else if indexPath.row == 4 {
            cell.iconImage.image = #imageLiteral(resourceName: "clouds")
            cell.detailsTitle.text = "Clouds"
            cell.detailsValue.text = "\(viewModel.weatherItem.clouds.all)%"
        } else if indexPath.row == 5 {
            cell.iconImage.image = #imageLiteral(resourceName: "wind")
            cell.detailsTitle.text = "Wind speed"
            cell.detailsValue.text = "\(viewModel.weatherItem.wind.speed) m/s"
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return 60
        }
        return 40
    }
    
    
}

// MARK: Setup UI
extension WeatherDetailsVC {
    
    fileprivate func setup() {
        
        // for UI test
        setValue("weatherDetails", forKey: Constants.accessibilityLabel)
        view.backgroundColor = .white
        navigationItem.title = viewModel.weatherDate
//        navigationController?.navigationItem.largeTitleDisplayMode = .never
        navigationController?.navigationBar.barTintColor = .white
        navigationController?.navigationBar.setValue("detailsNavigationBar", forKey: Constants.accessibilityLabel)
        
        setupMainUI()
    }
    
    private func setupMainUI() {
        
        let iconView: UIImageView = {
            let imageView = UIImageView()
            imageView.translatesAutoresizingMaskIntoConstraints = false
            imageView.tintColor = UIColor.foregroundColor
            imageView.contentMode = .scaleAspectFit
            imageView.image = UIImage(named: viewModel.weatherItem.weather[0].icon + "_large")
            return imageView
        }()
        view.addSubview(iconView)
        
        let temperature: UILabel = {
            let label = UILabel()
            label.translatesAutoresizingMaskIntoConstraints = false
            label.font = UIFont.helveticaBold(ofSize: 60)
            label.textColor = UIColor.foregroundColor
            label.textAlignment = .center
            label.text = viewModel.weatherItem.main.temperature.celcius
            label.alpha = 0
            return label
        }()
        view.addSubview(temperature)
        
        let weatherTitle: UILabel = {
            let label = UILabel()
            label.translatesAutoresizingMaskIntoConstraints = false
            label.font = UIFont.helveticaBold(ofSize: 24)
            label.textColor = UIColor.foregroundColor
            label.textAlignment = .center
            label.text = viewModel.weatherTitle
            label.alpha = 0
            return label
        }()
        view.addSubview(weatherTitle)
        
        let weatherDesc: UILabel = {
            let label = UILabel()
            label.translatesAutoresizingMaskIntoConstraints = false
            label.font = UIFont.helveticaRegular(ofSize: 18)
            label.textColor = UIColor.foregroundColor
            label.textAlignment = .center
            label.text = viewModel.weatherItem.weather[0].description.capitalized
            label.alpha = 0
            return label
        }()
        view.addSubview(weatherDesc)
        
        let detailsTableView: UITableView = {
            let tableView = UITableView()
            tableView.translatesAutoresizingMaskIntoConstraints = false
            tableView.delegate = self
            tableView.dataSource = self
            tableView.separatorStyle = .none
            tableView.backgroundColor = .clear
            tableView.tableFooterView = UIView()
            tableView.allowsSelection = false
            tableView.register(DetailsCell.self, forCellReuseIdentifier: Constants.detailsCellID)
            tableView.showsVerticalScrollIndicator = false
            tableView.alpha = 0
            return tableView
        }()
        view.addSubview(detailsTableView)
        
        
        // setup contraints
        
        let a = view.bounds.height
        
        let safeArea = view.safeAreaLayoutGuide
        iconView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        iconView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.1).isActive = true
        iconView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1).isActive = true
        iconView.topAnchor.constraint(equalTo: safeArea.topAnchor, constant: UIDevice.current.userInterfaceIdiom == .phone ? 50 : 100).isActive = true
        
        temperature.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        temperature.topAnchor.constraint(equalTo: iconView.bottomAnchor, constant: a).isActive = true
        
        weatherTitle.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        weatherTitle.topAnchor.constraint(equalTo: temperature.bottomAnchor, constant: 40).isActive = true
        
        weatherDesc.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        weatherDesc.topAnchor.constraint(equalTo: weatherTitle.bottomAnchor).isActive = true
        
        detailsTableView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        detailsTableView.widthAnchor.constraint(equalTo: safeArea.widthAnchor, multiplier: 0.7).isActive = true
        detailsTableView.topAnchor.constraint(equalTo: weatherDesc.bottomAnchor, constant: 40).isActive = true
        let tableConstraint = detailsTableView.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor, constant: a - 20)
        tableConstraint.isActive = true
        
        // MARK: Animations
        let animationDuration: TimeInterval = 1.5
        let springDamping: CGFloat = 0.8
        
        UIView.animate(withDuration: animationDuration, delay: 0, usingSpringWithDamping: springDamping, initialSpringVelocity: 0, options: .curveEaseOut, animations: {
            temperature.transform = CGAffineTransform(translationX: 0, y: -(a - 40))
            temperature.alpha = 1
        }, completion: nil)
        UIView.animate(withDuration: animationDuration, delay: 0.2, usingSpringWithDamping: springDamping, initialSpringVelocity: 0, options: .curveEaseOut, animations: {
            weatherTitle.transform = CGAffineTransform(translationX: 0, y: -(a - 40))
            weatherTitle.alpha = 1
        }, completion: nil)
        UIView.animate(withDuration: animationDuration, delay: 0.4, usingSpringWithDamping: springDamping, initialSpringVelocity: 0, options: .curveEaseOut, animations: {
            weatherDesc.transform = CGAffineTransform(translationX: 0, y: -(a - 40))
            weatherDesc.alpha = 1
        }, completion: nil)
        tableConstraint.constant = a - 40
        UIView.animate(withDuration: animationDuration, delay: 0.6, usingSpringWithDamping: springDamping, initialSpringVelocity: 0, options: .curveEaseOut, animations: {
            detailsTableView.transform = CGAffineTransform(translationX: 0, y: -(a - 40))
            detailsTableView.alpha = 1
            detailsTableView.layoutIfNeeded()
        }, completion: nil)
        
    }
    
}
