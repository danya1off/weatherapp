//
//  ViewController.swift
//  WeatherApp
//
//  Created by Jeyhun Danyalov on 7/11/18.
//  Copyright © 2018 Jeyhun Danyalov. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import CoreData
import CocoaLumberjack

class WeatherListTVC: UITableViewController, CLLocationManagerDelegate {

    private var locationManager: CLLocationManager!
    private var location: CLLocationCoordinate2D?
    private var activityIndocator = UIActivityIndicatorView(style: .gray)
    var viewModel = WeatherViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        getLocation()
        fetchData()
    }
    
    // Fetch all data if exists
    private func fetchData() {
        do {
            try viewModel.fetchData(self)
        } catch {
            handleError(error)
        }
    }
    
    // Request location permissions from user when app launched in first time
    private func getLocation() {
        
        locationManager = CLLocationManager()
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
            locationManager.startUpdatingLocation()
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard  let loc = manager.location?.coordinate else {
            return
        }
        manager.stopUpdatingLocation()
        manager.delegate = nil
        location = loc
    }
    
    // Save or update data in local storage after retrieving from remote API service
    @objc fileprivate func saveOrUpdateData() {
        if let location = location {
            activityIndocator.startAnimating()
//            refreshControl?.beginRefreshing()
            viewModel.getWeatherDataFromAPI(location: location) { [weak self] data in
                switch data {
                case .failure(let error):
                    DispatchQueue.main.async {
                        self?.activityIndocator.stopAnimating()
                        self?.refreshControl?.endRefreshing()
                    }
                    self?.handleError(error)
                case .success(_):
                    DispatchQueue.main.async {
                        self?.updateUI()
                    }
                }
            }
        } else {
            DDLogError("Error in getting location data")
            alertError(with: "Can not get any location data. Please, check Location setting!")
        }
    }
    
    // update UI in Main Thread after saving data and get it back
    private func updateUI() {
        UserDefaults.standard.set(true, forKey: Constants.dataRecievedFromAPI)
        fetchData()
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Update", style: .plain, target: self, action: #selector(saveOrUpdateData))
        activityIndocator.stopAnimating()
        tableView.reloadData()
        refreshControl?.endRefreshing()
    }

}

// MARK: UITableViewDelegate/UITableViewDataSource
extension WeatherListTVC {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfItemsIn(section: section)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.cellID, for: indexPath) as! CustomCell
        
        let cellViewModel = viewModel.getWeatherItem(at: indexPath)
        cell.viewModel = cellViewModel
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cellViewModel = viewModel.getWeatherItem(at: indexPath)
        let detailsViewController = WeatherDetailsVC(viewModel: cellViewModel)
        navigationController?.pushViewController(detailsViewController, animated: true)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { [weak self] (action, indexPath) in
            let alertController = UIAlertController(title: "Warning", message: "Are you sure to delete this weather data?", preferredStyle: .actionSheet)
            let deleteAction = UIAlertAction(title: "Delete", style: .destructive, handler: { action in
                self?.viewModel.deleteWeatherItem(at: indexPath)
            })
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alertController.addAction(deleteAction)
            alertController.addAction(cancelAction)
            self?.present(alertController, animated: true)
        }
        
        return [delete]
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return 150
        }
        return 300
    }
    
}

// MARK: NSFetchedResultsControllerDelegate
extension WeatherListTVC: NSFetchedResultsControllerDelegate {
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
    
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            if let indexPath = newIndexPath {
                tableView.insertRows(at: [indexPath], with: .fade)
            }
        case .update:
            if let indexPath = indexPath {
                tableView.reloadRows(at: [indexPath], with: .automatic)
            }
        case .delete:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
        default:
            break
        }
    }
}

// MARK: Setup UI
extension WeatherListTVC {
    
    fileprivate func setup() {
        view.backgroundColor = UIColor.backgroundColor
        // for UI testing
        setValue("weatherList", forKey: Constants.accessibilityLabel)
        view.addSubview(activityIndocator)
        activityIndocator.center = view.center
        setupTableView()
        setupNavigationBar()
    }
    
    private func setupNavigationBar() {
        
        viewModel.navigationTitle.bind { [weak self] title in
            self?.navigationItem.title = title
        }
        
        if UserDefaults.standard.bool(forKey: Constants.dataRecievedFromAPI) {
            navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Update", style: .plain, target: self, action: #selector(saveOrUpdateData))
        } else {
            navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(saveOrUpdateData))
        }
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.shadowImage = UIImage()
        
        // for UI testing
        navigationItem.rightBarButtonItem?.setValue("saveOrUpdateButton", forKey: Constants.accessibilityLabel)
        navigationItem.backBarButtonItem?.setValue("backButton", forKey: Constants.accessibilityLabel)
        navigationController?.navigationBar.setValue("tableNavigationBar", forKey: Constants.accessibilityLabel)
    }
    
    private func setupTableView() {
        tableView.backgroundColor = UIColor.backgroundColor
        tableView.register(CustomCell.self, forCellReuseIdentifier: Constants.cellID)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.separatorStyle = .none
        tableView.tableFooterView = UIView()
        tableView.showsVerticalScrollIndicator = false
        
        // for UI testing
        tableView.setValue("weatherTable", forKey: Constants.accessibilityLabel)
        
        // refresh control setup
        refreshControl = UIRefreshControl()
        refreshControl!.attributedTitle = NSAttributedString(string: "Pull to refresh weather data")
        refreshControl!.addTarget(self, action: #selector(saveOrUpdateData), for: .valueChanged)
        tableView.addSubview(refreshControl!)
    }
    
}

