//
//  CityEntity.swift
//  WeatherApp
//
//  Created by Jeyhun Danyalov on 7/13/18.
//  Copyright © 2018 Jeyhun Danyalov. All rights reserved.
//

import Foundation
import CoreData

class CityEntity: NSManagedObject {
    
    class func saveCity(data city: City, in context: NSManagedObjectContext) -> CityEntity {
        
        let cityEntity = CityEntity(context: context)
        cityEntity.id = Int32(city.id)
        cityEntity.name = city.name
        if let population = city.population {
            cityEntity.population = Int32(population)
        }
        cityEntity.country = city.country

        return cityEntity
    }
    
}
