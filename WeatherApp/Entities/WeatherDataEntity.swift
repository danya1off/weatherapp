//
//  WeatherDataEntity.swift
//  WeatherApp
//
//  Created by Jeyhun Danyalov on 7/13/18.
//  Copyright © 2018 Jeyhun Danyalov. All rights reserved.
//

import Foundation
import CoreData

class WeatherDataEntity: NSManagedObject {
    
    class func getOrSaveWeatherData(data weatherData: WeatherData, in context: NSManagedObjectContext) throws -> WeatherDataEntity {
        
        let fetchRequest: NSFetchRequest<WeatherDataEntity> = WeatherDataEntity.fetchRequest()
        do {
            let data = try context.fetch(fetchRequest)
            if data.count > 0 {
                return data[0]
            }
        } catch {
            throw error
        }
        
        
        let weatherDataEntity = WeatherDataEntity(context: context)
        weatherDataEntity.cod = weatherData.cod
        weatherDataEntity.count = Int32(weatherData.cnt)
        weatherDataEntity.requestDate = Date()
        
        let cityEntity = CityEntity.saveCity(data: weatherData.city, in: context)
        weatherDataEntity.city = cityEntity
        
        for item in weatherData.weatherItem {
            let weatherItem = WeatherItemEntity.saveWeatherItem(data: item, in: context)
            weatherDataEntity.addToWeatherItem(weatherItem)
        }
        
        return weatherDataEntity
    }
    
}
