//
//  WeatherEntity.swift
//  WeatherApp
//
//  Created by Jeyhun Danyalov on 7/13/18.
//  Copyright © 2018 Jeyhun Danyalov. All rights reserved.
//

import Foundation
import CoreData

class WeatherEntity: NSManagedObject {
    
    class func saveWeather(data weather: Weather, in context: NSManagedObjectContext) -> WeatherEntity {
        
        let weatherEntity = WeatherEntity(context: context)
        weatherEntity.id = Int32(weather.id)
        weatherEntity.main = weather.main
        weatherEntity.desc = weather.description
        weatherEntity.icon = weather.icon

        return weatherEntity
    }
    
}
