//
//  WeatherItemEntity.swift
//  WeatherApp
//
//  Created by Jeyhun Danyalov on 7/13/18.
//  Copyright © 2018 Jeyhun Danyalov. All rights reserved.
//

import Foundation
import CoreData

class WeatherItemEntity: NSManagedObject {
    
    class func saveWeatherItem(data item: WeatherItem, in context: NSManagedObjectContext) -> WeatherItemEntity {
        
        let weatherItemEntity = WeatherItemEntity(context: context)
        weatherItemEntity.date = Date.dateFromString(s: item.date)
        weatherItemEntity.temperature = item.main.temperature
        weatherItemEntity.minTemperature = item.main.minTemperature
        weatherItemEntity.maxTemperature = item.main.maxTemperature
        weatherItemEntity.groundLevel = item.main.groundLevel
        weatherItemEntity.humidity = Int32(item.main.humidity)
        weatherItemEntity.groundLevel = item.main.groundLevel
        weatherItemEntity.pressure = item.main.pressure
        weatherItemEntity.seaLevel = item.main.seaLevel
        weatherItemEntity.clouds = Int32(item.clouds.all)
        weatherItemEntity.windSpeed = item.wind.speed
        weatherItemEntity.windDeg = item.wind.deg
        
        let weather = WeatherEntity.saveWeather(data: item.weather[0], in: context)
        weatherItemEntity.weather = weather
        
        return weatherItemEntity
    }
    
}
