//
//  City.swift
//  WeatherApp
//
//  Created by Jeyhun Danyalov on 7/11/18.
//  Copyright © 2018 Jeyhun Danyalov. All rights reserved.
//

import Foundation

struct City: Codable {
    
    var id: Int
    var name: String
    var country: String
    var population: Int?
    
    static func convertToModel(data: CityEntity) -> City {
        return City(id: Int(data.id),
                    name: data.name ?? "",
                    country: data.country ?? "",
                    population: Int(data.population))
    }
    
}
