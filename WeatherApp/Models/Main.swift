//
//  Main.swift
//  WeatherApp
//
//  Created by Jeyhun Danyalov on 7/11/18.
//  Copyright © 2018 Jeyhun Danyalov. All rights reserved.
//

import Foundation

struct Main: Codable {
    var temperature: Float
    var minTemperature: Float
    var maxTemperature: Float
    var pressure: Float
    var seaLevel: Float
    var groundLevel: Float
    var humidity: Int
    var kfTemperature: Float
    
    private enum CodingKeys: String, CodingKey {
        case pressure, humidity
        case temperature = "temp"
        case minTemperature = "temp_min"
        case maxTemperature = "temp_max"
        case seaLevel = "sea_level"
        case groundLevel = "grnd_level"
        case kfTemperature = "temp_kf"
    }
}
