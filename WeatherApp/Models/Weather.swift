//
//  Weather.swift
//  WeatherApp
//
//  Created by Jeyhun Danyalov on 7/11/18.
//  Copyright © 2018 Jeyhun Danyalov. All rights reserved.
//

import Foundation

struct Weather: Codable {
    var id: Int
    var main: String
    var description: String
    var icon: String
    
    static func convertToModel(data: WeatherEntity) -> Weather {
        return Weather(id: Int(data.id),
                       main: data.main ?? "",
                       description: data.desc ?? "",
                       icon: data.icon ?? "")
    }
    
}
