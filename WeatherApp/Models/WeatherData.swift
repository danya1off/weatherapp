//
//  WeatherData.swift
//  WeatherApp
//
//  Created by Jeyhun Danyalov on 7/11/18.
//  Copyright © 2018 Jeyhun Danyalov. All rights reserved.
//

import Foundation

struct WeatherData: Codable {
    var cod: String
    var cnt: Int
    var requestDate = Date()
    var city: City
    var weatherItem: [WeatherItem]
    
    private enum CodingKeys: String, CodingKey {
        case cod, cnt, city
        case weatherItem = "list"
    }
    
    static func convertToModel(data: WeatherDataEntity) -> WeatherData {
        
        let city = City.convertToModel(data: data.city!)
        var weatherItems = [WeatherItem]()
        for item in data.weatherItem as! Set<WeatherItemEntity> {
            weatherItems.append(WeatherItem.convertToModel(data: item))
        }
        
        return WeatherData(cod: data.cod ?? "",
                           cnt: Int(data.count),
                           requestDate: data.requestDate ?? Date(),
                           city: city,
                           weatherItem: weatherItems)
    }
}
