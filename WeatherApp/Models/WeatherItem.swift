//
//  WeatherItem.swift
//  WeatherApp
//
//  Created by Jeyhun Danyalov on 7/11/18.
//  Copyright © 2018 Jeyhun Danyalov. All rights reserved.
//

import Foundation
struct WeatherItem: Codable {
    
    var date: String
    var main: Main
    var weather: [Weather]
    var clouds: Clouds
    var wind: Wind
    
    private enum CodingKeys: String, CodingKey {
        case main, weather, clouds, wind
        case date = "dt_txt"
    }
    
    static func convertToModel(data: WeatherItemEntity) -> WeatherItem {
        
        let main = Main(temperature: data.temperature,
                        minTemperature: data.minTemperature,
                        maxTemperature: data.maxTemperature,
                        pressure: data.pressure,
                        seaLevel: data.seaLevel,
                        groundLevel: data.groundLevel,
                        humidity: Int(data.humidity),
                        kfTemperature: 0.0)
        
        let weather = Weather.convertToModel(data: data.weather!)
        let clouds = Clouds(all: Int(data.clouds))
        let wind = Wind(speed: data.windSpeed, deg: data.windDeg)
        
        return WeatherItem(date: Date.stringFromDate(d: data.date ?? Date()),
                    main: main,
                    weather: [weather],
                    clouds: clouds,
                    wind: wind)
        
    }
    
}
