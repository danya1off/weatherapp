//
//  Wind.swift
//  WeatherApp
//
//  Created by Jeyhun Danyalov on 7/11/18.
//  Copyright © 2018 Jeyhun Danyalov. All rights reserved.
//

import Foundation

struct Wind: Codable {
    var speed: Float
    var deg: Float
}
