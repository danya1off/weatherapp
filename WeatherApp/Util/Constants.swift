//
//  Constants.swift
//  WeatherApp
//
//  Created by Jeyhun Danyalov on 7/11/18.
//  Copyright © 2018 Jeyhun Danyalov. All rights reserved.
//

import Foundation

class Constants {
    
    private init() {}
    
    public static var weatherAPIID = "caee89ee6762f46d9c8a868246ebc667"
    
    public static var dataRecievedFromAPI = "dataRecievedFromAPI"
    public static var accessibilityLabel = "accessibilityLabel"
    
    public static var cellID = "CustomCellID"
    public static var detailsCellID = "DetailsCellID"
    
}
