//
//  Enums.swift
//  WeatherApp
//
//  Created by Jeyhun Danyalov on 7/15/18.
//  Copyright © 2018 Jeyhun Danyalov. All rights reserved.
//

import Foundation

enum WeatherError: Error {
    case requestError(String)
    case dataError(String)
    case parsingError(String)
    case urlError(String)
}

enum WeatherResponse<T> {
    case success(T)
    case failure(WeatherError)
}
