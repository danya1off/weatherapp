//
//  Extensions.swift
//  WeatherApp
//
//  Created by Jeyhun Danyalov on 7/13/18.
//  Copyright © 2018 Jeyhun Danyalov. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func alertError(with message: String) {
        let alertController = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "Close", style: .cancel, handler: nil)
        alertController.addAction(alertAction)
        self.present(alertController, animated: true)
    }
    
    func handleError(_ error: Error) {
        if let error = error as? WeatherError {
            switch error {
            case let .urlError(errorMessage):
                alertError(with: errorMessage)
            case let .requestError(errorMessage):
                alertError(with: errorMessage)
            case let .parsingError(errorMessage):
                alertError(with: errorMessage)
            case let .dataError(errorMessage):
                alertError(with: errorMessage)
            }
        } else {
            alertError(with: error.localizedDescription)
        }
    }
    
}

extension Date {
    
    static func dateFromString(s: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter.date(from: s)!
    }
    
    static func stringFromDate(d: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy HH:mm"
        return dateFormatter.string(from: d)
    }
    
}

extension UIColor {
    
    static var backgroundColor: UIColor {
        return UIColor(red:0.93, green:0.93, blue:0.93, alpha:1.0)
    }
    
    static var foregroundColor: UIColor {
        return UIColor(red:0.44, green:0.44, blue:0.44, alpha:1.0)
    }
    
}

extension Float {
    
    var celcius: String {
        let formattedValue = Darwin.round(self * 10) / 10
        return "\(formattedValue)°"
    }
    
    var rounded: String {
        let formattedValue = Darwin.round(self * 10) / 10
        return "\(formattedValue)"
    }
    
}

extension UIFont {
    
    static func helveticaLight(ofSize size: CGFloat) -> UIFont {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return UIFont(name: "HelveticaNeue-Light", size: size * 2)!
        }
        return UIFont(name: "HelveticaNeue-Light", size: size)!
    }
    static func helveticaRegular(ofSize size: CGFloat) -> UIFont {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return UIFont(name: "HelveticaNeue", size: size * 2)!
        }
        return UIFont(name: "HelveticaNeue", size: size)!
    }
    static func helveticaMedium(ofSize size: CGFloat) -> UIFont {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return UIFont(name: "HelveticaNeue-Medium", size: size * 2)!
        }
        return UIFont(name: "HelveticaNeue-Medium", size: size)!
    }
    static func helveticaBold(ofSize size: CGFloat) -> UIFont {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return UIFont(name: "HelveticaNeue-Bold", size: size * 2)!
        }
        return UIFont(name: "HelveticaNeue-Bold", size: size)!
    }
    static func helveticaCondensedBlack(ofSize size: CGFloat) -> UIFont {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return UIFont(name: "HelveticaNeue-CondensedBlack", size: size * 2)!
        }
        return UIFont(name: "HelveticaNeue-CondensedBlack", size: size)!
    }
 
}
