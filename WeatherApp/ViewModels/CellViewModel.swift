//
//  CellViewModel.swift
//  WeatherApp
//
//  Created by Jeyhun Danyalov on 7/14/18.
//  Copyright © 2018 Jeyhun Danyalov. All rights reserved.
//

import Foundation

class CellViewModel {
    
    var weatherItem: WeatherItem!
    
    init(item: WeatherItem) {
        self.weatherItem = item
    }
    
    var weatherTitle: String {
        return weatherItem.weather[0].main
    }
    
    var weatherDate: String {
        return weatherItem.date
    }
    
}
