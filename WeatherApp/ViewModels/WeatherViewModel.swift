//
//  WeatherViewModel.swift
//  WeatherApp
//
//  Created by Jeyhun Danyalov on 7/11/18.
//  Copyright © 2018 Jeyhun Danyalov. All rights reserved.
//

import Foundation
import MapKit
import CoreData
import CocoaLumberjack

class WeatherViewModel {
    
    typealias ResponseData = (WeatherResponse<WeatherData>) -> Void
    var navigationTitle = Box("Unknown City")
    
    var session: URLSessionProtocol!
    var persistenceContainer: NSPersistentContainer!
    
    init(session: URLSessionProtocol  = URLSession.shared,
                 container: NSPersistentContainer = CustomContext.shared.persistentContainer) {
        self.session = session
        self.persistenceContainer = container
    }
    
    convenience init(session: URLSessionProtocol = URLSession.shared) {
        self.init(session: session, container: CustomContext.shared.persistentContainer)
    }
    
    convenience init(persistenceContainer: NSPersistentContainer) {
        self.init(session: URLSession.shared, container: persistenceContainer)
    }
    
    convenience init() {
        self.init(session: URLSession.shared, container: CustomContext.shared.persistentContainer)
    }
    
    private lazy var fetchedResultController: NSFetchedResultsController<WeatherItemEntity> = {
        let fetchRequest: NSFetchRequest<WeatherItemEntity> = WeatherItemEntity.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "temperature", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: CustomContext.shared.viewContext, sectionNameKeyPath: nil, cacheName: nil)
        return frc
    }()
    
    /**
     Fetch data from CoreData for populating UITableView
     Throw an error if there any error in data fetching
    */
    func fetchData(_ delegate: NSFetchedResultsControllerDelegate) throws {
        fetchedResultController.delegate = delegate
        do {
            try fetchedResultController.performFetch()
        } catch {
            DDLogError("Error in data fetching from DB!")
            throw WeatherError.dataError("Error in data fetching from DB!")
        }
    }
    
    /**
     Return the number of sections from NSFetchedResultController
     */
    var numberOfSections: Int {
        return fetchedResultController.sections?.count ?? 1
    }
    
    /**
     Return number of items in section
     - parameters:
         - section: The section's number
     
     - returns:
     The number of items in given section
    */
    func numberOfItemsIn(section: Int) -> Int {
        if let sections = fetchedResultController.sections {
            return sections[section].numberOfObjects
        }
        return 0
    }
    
    /**
     Get single weather item for given index path of UITableView
     
     - parameters:
         - indexPath: IndexPath of UITableView
     
     - returns:
     CellViewModel with weather item data
    */
    func getWeatherItem(at indexPath: IndexPath) -> CellViewModel {
        let entity = fetchedResultController.object(at: indexPath)
        let weatherItem = WeatherItem.convertToModel(data: entity)
        return CellViewModel(item: weatherItem)
    }
    
    /**
     Delete weather item for given index path of UITableView
     
     - parameters:
        - indexPath: IndexPath of UITableView
     
     */
    func deleteWeatherItem(at indexPath: IndexPath) {
        let tag = fetchedResultController.object(at: indexPath)
        CustomContext.shared.viewContext.delete(tag)
        CustomContext.shared.saveContext()
    }
    
    /**
     Get City name which weather data we fetched from API service
     
     - returns:
    City name as String
     */
    func getCityName() throws -> String {
        let fetchRequest: NSFetchRequest = CityEntity.fetchRequest()
        do {
            let fetchedData = try CustomContext.shared.viewContext.fetch(fetchRequest)
            if fetchedData.count > 0 {
                return fetchedData[0].name ?? "Unknown City"
            }
        } catch {
            DDLogError("Error in getting city name!")
            throw WeatherError.dataError("Error in getting city name!")
        }
        return "Unknown City"
    }
    
    /**
     Get weather data from remote API service.
     
     - parameters:
         - location: Location(latitude and longitude) of current location(city)
         - completionHandler: Closure with ResponseCompletion object
     
     Service from [OpenWeatherMap](https://openweathermap.org/api) .
     In this function we just generate URL and then call apiCall(with url: URL, completion: @escaping(ResponseCompletion))
     function.
     After retrieving the data and if there are no any errors, then we save all data
     in local storage in CoreData.
     I didn't used any thirt part libraries or frameworks as Alamofire for Rest of Realm for local storage,
     because CoreData from iOS 10 is great and really easy for use, and URLSession is great for quick API requests
    */
    func getWeatherDataFromAPI(location: CLLocationCoordinate2D, completion: @escaping(ResponseData)) {
        let urlString = "https://api.openweathermap.org/data/2.5/forecast?lat=\(location.latitude)&lon=\(location.longitude)&mode=json&units=metric&cnt=7&appid=\(Constants.weatherAPIID)"
        guard let url = URL(string: urlString) else {
            DDLogError("Error in request url!")
            completion(.failure(.urlError("Error in request url!")))
            return
        }
        apiCall(with: url) { [weak self] result in
            switch result {
            case let .failure(error):
                completion(.failure(error))
            case let .success(weatherData):
                self?.saveWeatherData(weatherData) { data in
                    completion(data)
                }
            }
        }
    }
    
    /**
     Call API service for retrieve weather data
     
     - parameters:
         - url: URL string for API service
         - completionHandler: Closure with ResponseConpletion object
     
     In this function we call remote API service and parse retrieved data via Decode protocol
     to WeatherData class.
    */
    func apiCall(with url: URL, completion: @escaping(ResponseData)) {
        session.getData(with: url) { (data, error) in
            if let err = error {
                DDLogError("Error in response - \(err)")
                completion(.failure(.requestError("Error in response - \(err)")))
            } else {
                guard let data = data else {
                    DDLogError("Can not retrieve any data from API!")
                    completion(.failure(.dataError("Can not retrieve any data from API!")))
                    return
                }
                do {
                    let decoder = JSONDecoder()
                    decoder.nonConformingFloatDecodingStrategy = .throw
                    let weatherData = try decoder.decode(WeatherData.self, from: data)
                    completion(.success(weatherData))

                } catch {
                    DDLogError("Error in data parsing!")
                    completion(.failure(.parsingError("Error in data parsing!")))
                }
            }
        }
    }
    
    /**
     Save all retrieved data from API to local storage. Used CoreData
     
     - parameters:
         - data: WeatherData object
         - completionHandler: Closure with ResponseConpletion object
     
     I save all data in background thread with special function of NSPersistenContainer
     performBackgroundTask, because it's great if we have huge amount of data for
     saving in local storage without freezing our UI. We return saved data via
     completionHandler
    */
    func saveWeatherData(_ data: WeatherData, completion: @escaping (ResponseData)) {
        persistenceContainer.performBackgroundTask { [weak self] context in
            
            if UserDefaults.standard.bool(forKey: Constants.dataRecievedFromAPI) {
                let fetchRequest: NSFetchRequest<WeatherDataEntity> = WeatherDataEntity.fetchRequest() 
                do {
                    let data = try context.fetch(fetchRequest)
                    for item in data {
                        context.delete(item)
                    }
                } catch {
                    DDLogError("Error in data fetching!")
                    completion(.failure(.dataError("Error in data fetching!")))
                }
            }
            
            let weatherDataEntity = try? WeatherDataEntity.getOrSaveWeatherData(data: data, in: context)
            do {
                try context.save()
            } catch {
                DDLogError("Error in data saving to DB!")
                completion(.failure(.dataError("Error in data saving to DB!")))
            }
            
            guard let data = weatherDataEntity else {
                DDLogError("Can not retrieve any data from DB!")
                completion(.failure(.dataError("Can not retrieve any data from DB!")))
                return
            }
            let weatherData = WeatherData.convertToModel(data: data)
            // Update NavigationBar's title 
            DispatchQueue.main.async {
                self?.navigationTitle.value = weatherData.city.name
            }
            completion(.success(weatherData))
        }
        
    }
    
}

// MARK: URLSessionProtocol
// Protocol for depency injection and Unit testing

protocol URLSessionProtocol {
    typealias ResponseData = (Data?, Error?) -> Void
    func getData(with url: URL, completion: @escaping ResponseData)
}

extension URLSession: URLSessionProtocol {
    
    func getData(with url: URL, completion: @escaping URLSessionProtocol.ResponseData) {
        let task = dataTask(with: url) { (data, _, error) in
            completion(data, error)
        }
        task.resume()
    }
    
}

class URLSessionMock: URLSessionProtocol {
    
    let data: Data? = nil
    let error: Error? = nil
    var url: URL? = nil
    
    func getData(with url: URL, completion: @escaping URLSessionProtocol.ResponseData) {
        self.url = url
        completion(data, error)
    }
    
}










