//
//  WeatherAppTests.swift
//  WeatherAppTests
//
//  Created by Jeyhun Danyalov on 7/11/18.
//  Copyright © 2018 Jeyhun Danyalov. All rights reserved.
//

import XCTest
import CoreData
import CocoaLumberjack
@testable import WeatherApp

class WeatherAppTests: XCTestCase {
    
    var viewModel: WeatherViewModel!
    
    // Mock for URLSession
    let session = URLSessionMock()
    
    // Mock for CoreData
    lazy var managedObject: NSManagedObjectModel = {
        let managedObject = NSManagedObjectModel.mergedModel(from: [Bundle(for: type(of: self))] )!
        return managedObject
    }()
    
    lazy var mockPersistense: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "WeatherApp", managedObjectModel: managedObject)
        let description = NSPersistentStoreDescription()
        description.type = NSInMemoryStoreType
        description.shouldAddStoreAsynchronously = false
        container.persistentStoreDescriptions = [description]
        container.loadPersistentStores(completionHandler: { (description, error) in
            precondition(description.type == NSInMemoryStoreType)
            if let error = error {
                fatalError("Fatal error occured!")
            }
        })
        return container
    }()
    
    override func setUp() {
        super.setUp()
        viewModel = WeatherViewModel(session: session, container: mockPersistense)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testApiCall() {
        let sampleURL = URL(string: "http://mock.url")!
        viewModel.apiCall(with: sampleURL) { _ in }
        
        XCTAssert(sampleURL == session.url)
    }
    
    func testSaveWeatherData() {
        
        let city = City(id: 1, name: "Baku", country: "AZ", population: 11000000)
        let main = Main(temperature: 26, minTemperature: 26, maxTemperature: 26, pressure: 1010, seaLevel: 1020, groundLevel: 1000, humidity: 90, kfTemperature: 2)
        let weather = Weather(id: 804, main: "Clouds", description: "overcast clouds", icon: "04n")
        let clouds = Clouds(all: 10)
        let wind = Wind(speed: 3, deg: 145)
        let weatherItem = WeatherItem(date: "2018-07-16 21:00:00", main: main, weather: [weather], clouds: clouds, wind: wind)
        let weatherData = WeatherData(cod: "200", cnt: 1, requestDate: Date(), city: city, weatherItem: [weatherItem])
        
        viewModel.saveWeatherData(weatherData) { _ in }
        
        let fetchRequest: NSFetchRequest<WeatherDataEntity> = WeatherDataEntity.fetchRequest()
        let data = try? mockPersistense.viewContext.fetch(fetchRequest)
        XCTAssert(data != nil)
        
    }
    
}
