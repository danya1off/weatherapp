//
//  WeatherAppUITests.swift
//  WeatherAppUITests
//
//  Created by Jeyhun Danyalov on 7/11/18.
//  Copyright © 2018 Jeyhun Danyalov. All rights reserved.
//

import XCTest

class WeatherAppUITests: XCTestCase {
    
    var app: XCUIApplication!
    
    override func setUp() {
        super.setUp()
        
        app = XCUIApplication()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testDidSelectCellAtIndexPath() {

        let tableView = app.tables["weatherTable"]
        tableView.cells.element(boundBy: 0).tap()
        
        app.navigationBars["detailsNavigationBar"].children(matching: .button)
        .matching(identifier: "backButton").element(boundBy: 0).tap()
        
    }
    
    func testWeatherItemsCount() {
        let table = app.tables["weatherTable"]
        XCTAssert(table.cells.count > 0)
    }
    
    func testSaveOrUpdateButton() {
        
        let tableView = app.tables["weatherTable"]
        XCTAssert(tableView.cells.count == 0)
        
        app.navigationBars["tableNavigationBar"].children(matching: .button)
        .matching(identifier: "saveOrUpdateButton").element(boundBy: 0).tap()
        sleep(2)
        XCTAssert(tableView.cells.count > 0)
        
    }
    
}
